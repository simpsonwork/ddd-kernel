<?php
/**
 * Created by simpson <simpsonwork@gmail.com>
 * Date: 18.04.17
 * Time: 18:55
 */

namespace DDD\Kernel;

interface Identifier
{
    /**
     * Generate a new Identifier
     *
     * @return Identifier
     */
    public static function generate(): Identifier;

    /**
     * Creates an Identifier from native value
     *
     * @param $native
     *
     * @return Identifier
     */
    public static function fromNative($native): Identifier;

    /**
     * Determine equality with another Identifier
     *
     * @param Identifier $other
     *
     * @return bool
     */
    public function equals(Identifier $other): bool;

    /**
     * Return Identifier as a string
     *
     * @return string
     */
    public function toString(): string;
}
