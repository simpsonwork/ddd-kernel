<?php
/**
 * Created by simpson <simpsonwork@gmail.com>
 * Date: 21.04.17
 * Time: 23:01
 */

namespace DDD\Kernel;

interface ValueObject
{
    /**
     * @param mixed $native
     *
     * @return ValueObject
     */
    public static function fromNative($native): ValueObject;

    /**
     * @param ValueObject $other
     *
     * @return bool
     */
    public function equals(ValueObject $other): bool;
}
