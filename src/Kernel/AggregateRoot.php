<?php
/**
 * Created by simpson <simpsonwork@gmail.com>
 * Date: 18.04.17
 * Time: 18:55
 */

namespace DDD\Kernel;

interface AggregateRoot
{
    /**
     * Return aggregate root identifier
     *
     * @return Identifier
     */
    public function id(): Identifier;

    /**
     * Add an event to the queue
     *
     * @param $event
     *
     * @return void
     */
    public function recordEvent($event);

    /**
     * Release the events
     *
     * @return array
     */
    public function releaseEvents(): array;
}
