<?php
/**
 * Created by simpson <simpsonwork@gmail.com>
 * Date: 21.04.17
 * Time: 22:40
 */

namespace DDD\Kernel;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class UuidIdentifier implements Identifier
{
    /**
     * @var UuidInterface
     */
    protected $uuid;

    /**
     * UuidIdentifier constructor.
     *
     * @param UuidInterface $uuid
     */
    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @inheritDoc
     */
    public static function generate(): Identifier
    {
        return new static(Uuid::uuid4());
    }

    /**
     * @inheritDoc
     */
    public static function fromNative($native): Identifier
    {
        return new static(Uuid::fromString($native));
    }

    /**
     * @inheritDoc
     */
    public function equals(Identifier $other): bool
    {
        return $this === $other;
    }

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return $this->uuid->toString();
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return $this->toString();
    }
}
