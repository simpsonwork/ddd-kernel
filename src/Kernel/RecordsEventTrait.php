<?php
/**
 * Created by simpson <simpsonwork@gmail.com>
 * Date: 18.04.17
 * Time: 19:34
 */

namespace DDD\Kernel;

trait RecordsEventTrait
{
    /**
     * @var array
     */
    private $events = [];

    /**
     * Record an occurred event
     *
     * @param $event
     *
     * @return void
     */
    public function recordEvent($event)
    {
        $this->events[] = $event;
    }

    /**
     * Release the events queue
     *
     * @return array
     */
    public function releaseEvents(): array
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }
}
