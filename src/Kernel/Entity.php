<?php
/**
 * Created by simpson <simpsonwork@gmail.com>
 * Date: 21.04.17
 * Time: 23:07
 */

namespace DDD\Kernel;

interface Entity
{
    /**
     * @return Identifier
     */
    public function id();
}
