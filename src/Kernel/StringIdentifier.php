<?php
/**
 * Created by simpson <simpsonwork@gmail.com>
 * Date: 21.04.17
 * Time: 22:37
 */

namespace DDD\Kernel;

use Ramsey\Uuid\Uuid;

abstract class StringIdentifier implements Identifier
{
    /**
     * @var string
     */
    protected $id;

    /**
     * StringIdentifier constructor.
     *
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @inheritDoc
     */
    public static function generate(): Identifier
    {
        return new static(Uuid::uuid4()->toString());
    }

    /**
     * @inheritDoc
     */
    public static function fromNative($native): Identifier
    {
        return new static((string) $native);
    }

    /**
     * @inheritDoc
     */
    public function equals(Identifier $other): bool
    {
        return $this === $other;
    }

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return $this->toString();
    }
}
